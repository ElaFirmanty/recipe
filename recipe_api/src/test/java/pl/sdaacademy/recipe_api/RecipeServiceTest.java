package pl.sdaacademy.recipe_api;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class RecipeServiceTest {

    @TestConfiguration
    static class RecipeServiceImplTestContextConfiguration {

        @Bean
        public RecipeService recipeService(
                RecipeRepository recipeRepository,
                RecipeUpdater updater) {
            return new RecipeService(recipeRepository,
                    updater,
                    null);
        }
    }

    @TestConfiguration
    static class RecipeServiceUpdater {
        @Bean
        public RecipeUpdater recipeUpdater(
                RecipeRepository recipeRepository) {
            return new RecipeUpdater(recipeRepository);
        }
    }

    @Autowired
    private RecipeService recipeService;

    @MockBean
    private RecipeRepository recipeRepository;

    @Test
    void when_update_empty_object_then_no_interaction_with_repo_should_be_expected() {
        //given
        Recipe recipeToReturn = new Recipe();
        recipeToReturn.setName("test");
        Recipe recipe = new Recipe();
        when(recipeRepository.findById(anyString())).thenReturn(Optional.of(recipeToReturn));

        //when
        Recipe updatedRecipe = recipeService.update("", recipe);

        //then
        assertEquals(recipeToReturn.getName(), updatedRecipe.getName());
        verify(recipeRepository).save(any());
    }

    @Test
    void when_find_by_name_which_not_existing_in_repo_then_return_exception(){
        //given
        String name = "pierogi";
        when(recipeRepository.findById(name))
                .thenReturn(Optional.ofNullable(null));

        //when
        //then
        assertThrows(NoRecipeException.class, ()->{
            recipeService.findByName(name);
        });
    }

    @Test
    void when_find_by_name_which_existing_in_repo_then_return_result(){
        //given
        String name = "pierogi";
        Recipe recipeToReturn = new Recipe("pierogi",
                "pierogi ruskie",
                Arrays.asList("ziemniaki", "twaróg"),
                60,
                2000,
                2);
        when(recipeRepository.findById(name))
                .thenReturn(Optional.of(recipeToReturn));

        //when
        List<Recipe> results = recipeService.findByName(name);

        //then
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals("pierogi", results.get(0).getName());
    }

    @Test
    void when_provide_name_of_item_existing_in_repo_then_item_should_be_removed(){
        //given
        String name = "pierogi";
        Recipe recipeToReturn = new Recipe("pierogi",
                "pierogi ruskie",
                Arrays.asList("ziemniaki", "twaróg"),
                60,
                2000,
                2);
        when(recipeRepository.findById(name))
                .thenReturn(Optional.of(recipeToReturn));
        //when
        recipeService.removeRecipe(name);

        //then
        verify(recipeRepository).delete(recipeToReturn);
        //dowolny obiekt klasy receipe przekazany do metody delete
        //verify(recipeRepository).delete(any(Recipe.class));
    }

    @Test
    void when_provide_existing_id_of_item_then_item_should_be_updated() {
        //given
        String name = "kopytka";
        Recipe recipeFromRepo = new Recipe("kopytka",
                "kopytka Jana",
                Arrays.asList("ziemniaki"),
                60,
                2000,
                2);
        Recipe recipeToUpdate = new Recipe("kopytka",
                "kopytka z Jana",
                Arrays.asList("ziemniaki", "twaróg"),
                60,
                2000,
                2);
        when(recipeRepository.findById(name))
                .thenReturn(Optional.of(recipeFromRepo));

        //when
        Recipe result = recipeService.update(name, recipeToUpdate);

        //then
        verify(recipeRepository).save(any(Recipe.class));
        assertEquals(recipeToUpdate.getName(), result.getName());
        assertEquals(recipeToUpdate.getDescription(), result.getDescription());
        assertEquals(recipeToUpdate.getIngredients(), result.getIngredients());
    }

}