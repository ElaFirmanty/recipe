package pl.sdaacademy.recipe_api;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class RecipeRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private RecipeRepository recipeRepository;

    @Test
    void when_get_all_then_return_emptyList(){
        //given

        //when
        List<Recipe> recipes = recipeRepository.findAll();

        //then
        assertTrue(recipes.isEmpty());
    }

    @Test
    void when_find_by_calories_then_one_record_from_db_should_be_returned() {
        //given
        Recipe recipe = new Recipe("kopytka", "kopytka od mamy", Arrays.asList("zmieniaki", "twaróg"), 2000, 60, 4);
        testEntityManager.persist(recipe);

        //when
        List<Recipe> results = recipeRepository.findByCalories(2000);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals("kopytka", results.get(0).getName());
    }

}