package pl.sdaacademy.recipe_api;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, String> {

    List<Recipe> findByCalories(int calories);
}
