package pl.sdaacademy.recipe_api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/recipe")
@RestController
public class RecipeController {
    private final RecipeService recipeService;
    @Autowired
    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    //localhost:8080/recipe
    @PostMapping
    public String addRecipe(@Valid @RequestBody Recipe recipe) {
        return recipeService.addRecipe(recipe);
    }
    //localhost:8080/recipe
    @GetMapping
    public List<Recipe> getAll() {
        return recipeService.getAll();
    }

    @GetMapping("/presentation")
    public List<RecipeDTO> getAllShortForm() {
        return recipeService.getAllShortForm();
    }

    //localhost:8080/recipe/findByName?name=...
    @GetMapping("/findByName")
    public List<Recipe> findByName(@RequestParam String name) {
        return recipeService.findByName(name);
    }

    @GetMapping("/findByCalories")
    public List<Recipe> findByCalories(@RequestParam String calories) {
        return recipeService.findByCalories(calories);
    }

    @GetMapping("/{name}")
    public Recipe getByName(@PathVariable String name) {
        return recipeService.getByName(name);
    }

    @DeleteMapping("/{name}")
    public Recipe remove(@PathVariable String name) {
        return recipeService.removeRecipe(name);
    }

    @PutMapping("/{name}")
    public Recipe update(@PathVariable String name,
                         @RequestBody Recipe recipeToUpdate) {
        return recipeService.update(name, recipeToUpdate);
    }

}

