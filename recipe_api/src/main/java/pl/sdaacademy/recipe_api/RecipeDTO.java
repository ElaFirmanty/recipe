package pl.sdaacademy.recipe_api;

public class RecipeDTO {
    private String name;
    private String description;

    public RecipeDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public RecipeDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
