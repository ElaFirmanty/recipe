package pl.sdaacademy.recipe_api;

import org.springframework.stereotype.Component;

@Component
public class RecipeTransformer {

    public RecipeDTO toDto(Recipe recipe) {
        RecipeDTO recipeDTO = new RecipeDTO(recipe.getName(),
                recipe.getDescription());
        return recipeDTO;
    }
}
