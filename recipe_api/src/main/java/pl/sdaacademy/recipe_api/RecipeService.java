package pl.sdaacademy.recipe_api;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecipeService {

    private final RecipeRepository recipeRepository;
    private final RecipeUpdater recipeUpdater;
    private final ModelMapper modelMapper;

    @Autowired
    public RecipeService(RecipeRepository recipeRepository,
                         RecipeUpdater recipeUpdater,
                         ModelMapper modelMapper) {
        this.recipeRepository = recipeRepository;
        this.recipeUpdater = recipeUpdater;
        this.modelMapper = modelMapper;
    }

    public String addRecipe(Recipe recipe) {
        recipeRepository.save(recipe);
        return "dodano";
    }

    public List<Recipe> getAll() {
        return recipeRepository.findAll();
    }

    public List<RecipeDTO> getAllShortForm() {
        return recipeRepository.findAll()
                .stream()
                .map(recipe -> {
                    return modelMapper.map(recipe, RecipeDTO.class);
                })
                .collect(Collectors.toList());
    }

    public List<Recipe> findByName(String name) {
        return Arrays.asList(recipeRepository.findById(name)
                .orElseThrow(()->{
                    throw new NoRecipeException("");
                }));
    }

    public List<Recipe> findByCalories(String calories) {
        return recipeRepository.findByCalories(Integer.parseInt(calories));
    }

    public Recipe getByName(String name) {
        return recipeRepository.findById(name)
                .orElseThrow(()->{
                    throw new NoRecipeException(name);
                });
    }

    public Recipe removeRecipe(String name) {
        Recipe recipe = recipeRepository.findById(name)
                .orElseThrow(()->{
                    throw new NoRecipeException(name);
                });
        recipeRepository.delete(recipe);
        return recipe;
        //recipeRepository.deleteById(name);
    }

    public Recipe update(String name, Recipe recipeToUpdate) {
        return recipeUpdater.update(name, recipeToUpdate);
    }
}
