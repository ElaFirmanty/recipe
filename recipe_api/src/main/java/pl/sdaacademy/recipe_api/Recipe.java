package pl.sdaacademy.recipe_api;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Recipe {
    //name - nie był pusty
    //name mieścił się w przedziale: 2-40 znaków
    @NotEmpty
    @Size(min = 2, max = 40)
    @Id
    private String name;
    //może być pusty
    //nie może zawierać białych znaków
    //przedział znaków od 10 do 100 znaków
    @NotBlank
    @Size(min = 10, max = 100)
    private String description;
    //nie może być nullem
    @NotNull
    @Convert(converter = StringArrayToStringConverter.class)
    private List<String> ingredients;
    //nie może być większa nić 20000 kcal
    @Max(20000)
    private int calories;
    @Min(1)
    @Max(10)
    //przedział od 1-10 osób
    private int numberOfPeople;
    //przedział od 15 min do 4h
    @Min(15)
    @Max(240)
    private int duration;

    public Recipe() {
    }

    public Recipe(String name, String description, List<String> ingredients, int calories, int duration, int numberOfPeople) {
        this.name = name;
        this.description = description;
        this.ingredients = ingredients;
        this.duration = duration;
        this.calories = calories;
        this.numberOfPeople = numberOfPeople;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return calories == recipe.calories &&
                numberOfPeople == recipe.numberOfPeople &&
                duration == recipe.duration &&
                Objects.equals(name, recipe.name) &&
                Objects.equals(description, recipe.description) &&
                Objects.equals(ingredients, recipe.ingredients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, ingredients, calories, numberOfPeople, duration);
    }
}
