package pl.sdaacademy.recipe_api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RecipeUpdater {

    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeUpdater(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public Recipe update(String name, Recipe recipeToUpdate) {
        Recipe recipe;
        recipe = recipeRepository.findById(name)
                .orElseThrow(() -> {
                    throw new NoRecipeException(name);
                });
        if (recipeToUpdate.getName() != null) {
            recipe.setName(recipeToUpdate.getName());
        }
        if (recipeToUpdate.getDescription() != null) {
            recipe.setDescription(recipeToUpdate.getDescription());
        }
        if (recipeToUpdate.getCalories() != 0) {
            recipe.setCalories(recipeToUpdate.getCalories());
        }
        if (recipeToUpdate.getDuration() != 0) {
            recipe.setDuration(recipeToUpdate.getDuration());
        }
        if (recipeToUpdate.getNumberOfPeople() != 0) {
            recipe.setNumberOfPeople(recipeToUpdate.getNumberOfPeople());
        }
        if (recipeToUpdate.getIngredients() != null) {
            recipe.setIngredients(recipeToUpdate.getIngredients());
        }
        recipeRepository.save(recipe);
        return recipe;
    }

}